extends CharacterBody2D

@onready var anim = $AnimatedSprite2D
@onready var jump_sfx = $JumpSfx
@onready var run_sfx = $RunSfx
@onready var root = get_tree().get_root()
@onready var projectile = preload("res://Shared/projectile.tscn")

const SPEED = 150.0
const JUMP_VELOCITY = -300.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")


func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("Jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
		jump_sfx.play()
		run_sfx.stop()

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("Move Left", "Move Right")
	if direction:
		velocity.x = direction * SPEED
		
		if !run_sfx.playing && is_on_floor():
			run_sfx.play()
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		run_sfx.stop()

	if not is_on_floor():
		anim.play("Jump")
	else:
		if velocity.x != 0:
			anim.play("Run")
		else:
			anim.play("Idle")
			
	if velocity.x > 0:
			anim.flip_h = false
	elif velocity.x < 0:
			anim.flip_h = true

	check_fire()

	move_and_slide()

func check_fire():
	if Input.is_action_just_pressed("Fire") && GameManager.try_spend_coins(1):
		var instance = projectile.instantiate()
		instance.spawnPosition = global_position
		instance.direction = -1 if anim.flip_h else 1
		root.add_child.call_deferred(instance)
		add_collision_exception_with(instance)
