extends CharacterBody2D

@onready var death_sfx = $DeathSfx
@onready var anim = $AnimatedSprite2D

@export var patrol_area: Area2D
@export var damage: int = 10

const SPEED = 40.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var direction = 1

func _ready():
	anim.flip_h = true
	patrol_area.body_exited.connect(flip)

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	velocity.x = direction * SPEED

	move_and_slide()
	
func flip(body):
	if body.name == 'Ghost':
		direction = direction * -1
		anim.flip_h = !anim.flip_h

func _on_death_area_body_entered(body):
	if body.name == "Player":
		GameManager.on_coin_collected(damage)
		death_sfx.finished.connect(delete)
		death_sfx.play()
		visible = false

	check_projectile_damage(body)

func _on_attack_area_body_entered(body):
	if body.name == "Player":
		GameManager.on_damage(damage)

	check_projectile_damage(body)

func delete():
	queue_free()

func check_projectile_damage(body):
	if body.name == "Projectile":
		GameManager.on_coin_collected(damage / 2)
		body.queue_free()
		death_sfx.finished.connect(delete)
		death_sfx.play()
		visible = false
