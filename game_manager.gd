extends Node

signal update_coins(new_amount)
signal update_health(new_amount)

var coins = 0
var health = 100

var save_location = "user://savegame.save"

func _ready():
	update_coins.connect(on_coin_updated)
	update_health.connect(on_health_updated)

func on_heart_collected():
	update_health.emit(100)
	
func on_damage(amount):
	update_health.emit(health - amount)

func on_coin_collected(amount = 1):
	update_coins.emit(coins + amount)

func on_coin_updated(new_amount):
	coins = new_amount
	print('You have collected a coin! You now have ' + str(coins))

func try_spend_coins(amount) -> bool:
	if coins >= amount:
		update_coins.emit(coins - amount)
		return true
	else:
		return false

func on_health_updated(new_amount):
	health = new_amount
	
	if health <= 0:
		print('You have died')
	else:
		print('Your health has changed to ' + str(health))

func save_game(current_coins: int, current_health: int, current_level_path: String):
	var file = FileAccess.open(save_location, FileAccess.WRITE)
	file.store_var(current_coins)
	file.store_var(current_health)
	file.store_line(current_level_path)

func load_game():
	if not FileAccess.file_exists(save_location):
		return

	var file = FileAccess.open(save_location, FileAccess.READ)
	update_coins.emit(file.get_var(coins))
	update_health.emit(file.get_var(health))
	get_tree().change_scene_to_file(file.get_line())

func delete_save():
	DirAccess.remove_absolute(save_location)
	
func reset_game():
	delete_save()
	coins = 0
	health = 100

func load_level(next_level: PackedScene):
	save_game(coins, health, next_level.resource_path)
	get_tree().change_scene_to_packed(next_level)
