extends Control

@onready var score_label = $VBoxContainer/HBoxContainer/ScoreLabel

func _ready():
	score_label.text = str(GameManager.coins)
	GameManager.reset_game()

func _on_menu_button_pressed():
	get_tree().change_scene_to_file("res://Levels/title_screen.tscn")
