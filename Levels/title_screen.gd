extends Control

@onready var click_sfx = $ClickSfx
@onready var load_button = $Buttons/LoadButton

func _ready():
	if FileAccess.file_exists(GameManager.save_location):
		load_button.show()
	else:
		load_button.hide()

func _on_play_button_pressed():
	click_sfx.finished.connect(play)
	click_sfx.play()

func _on_quit_button_pressed():
	click_sfx.finished.connect(quit)
	click_sfx.play()

func play():
	GameManager.reset_game()
	get_tree().change_scene_to_file("res://Levels/level_1.tscn")

func quit():
	get_tree().quit()

func _on_load_button_pressed():
	GameManager.load_game()
