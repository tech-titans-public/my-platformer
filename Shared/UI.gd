extends CanvasLayer

@onready var coin_label = $CoinContainer/CoinLabel
@onready var heart_label = $HealthContainer/HeartLabel

func _ready():
	GameManager.update_coins.connect(on_update_coins)
	on_update_coins(GameManager.coins)
	
	GameManager.update_health.connect(on_update_health)
	on_update_health(GameManager.health)

func on_update_coins(new_amount):
	coin_label.text = str(new_amount)

func on_update_health(new_amount):
	heart_label.text = str(new_amount)
