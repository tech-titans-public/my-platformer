extends CharacterBody2D

const SPEED = 200

var direction: float
var spawnPosition: Vector2

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity") / 2

func _ready():
	global_position = spawnPosition
	
func _physics_process(delta):
	if not is_on_floor():
		velocity.y += gravity * delta

	velocity.x = SPEED * direction

	move_and_slide()

func _on_timer_timeout():
	queue_free()
