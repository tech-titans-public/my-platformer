extends Area2D

@onready var pickup_sfx = $PickupSfx

func _on_body_entered(body):
	if body.name == "Player":
		GameManager.on_coin_collected()
		pickup_sfx.finished.connect(delete)
		pickup_sfx.play()
		visible = false

func delete():
	queue_free()
