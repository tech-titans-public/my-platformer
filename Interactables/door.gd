extends Area2D

@export var next_level: PackedScene

func _on_body_entered(_body):
	call_deferred('change_level')

func change_level():
	GameManager.load_level(next_level)
